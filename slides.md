---
# vim: set filetype=revealjs:
# vim: set syntax=markdown:
title: "`Git` worktree :cactus:"
subtitle: "Collaboration go brrrrr"
author: "[Pablo COVES](https://pcoves.gitlab.io)"
date: "
Tupper`Git` :: 2023-06-29
<br />
<img src='./licence.png' style='height: 2em;' />
"
---

# Context

![](https://thumbs.gfycat.com/AccurateSentimentalCat-size_restricted.gif)

::: notes
* Problem
* Approaches
    * Solution
    * Demonstration
* Conclusion
    * See also
:::

## You're working on

* The next *killer feature*
* A tricky *profiling issue*
* Updating the codebase *dependencies*

::: notes
Anything that takes time and/or implies a lot of changes
* Update `CI/CD`
* Update `.lock` file
:::

## Someone asks you to

* Troubleshoot their *branch*
* Add an important *quickfix*
* Make a *working demonstration*

::: notes
Anything that requires switching to another branch
:::

# Solution

![](https://media.tenor.com/YyurbHUFliUAAAAC/i-got-a-solution-idiocracy.gif)

::: notes
* There are multiple solutions to this problem
* They all have their pros and cons
* You should pick the one that fits your needs
:::

## `Git stash`

* :heavy_plus_sign: Quick and easy
* :heavy_minus_sign: May fail to restore

::: notes
`Git stash pop` may fail because of unexpected conflicts
:::

## `Git commit`

* :heavy_plus_sign: Quick and safe
* :heavy_minus_sign: Dirty the history

::: notes
`Git commit -am WIP` that will later need a *rewrite* and a `push --force`
:::

## `Git clone`

* :heavy_plus_sign: Clean and safe
* :heavy_minus_sign: Unrelated `git` trees

::: notes
Can't cherry-pick between two unrelated *trees*  without manual synchronization
:::

## `Git worktree`

* :heavy_plus_sign: Quick
* :heavy_plus_sign: Clean
* :heavy_plus_sign: Safe
* :heavy_minus_sign: Involved

### Take a `git` repository

```
└── [drwxr-xr-x]  repository
    ├── [drwxr-xr-x]  .git
    ├── [-rw-r--r--]  file0
    ├── [-rw-r--r--]  file1
    ├── [-rw-r--r--]  file2
    └── [-rw-r--r--]  file3
```

### With multiple *branches*

```
❯ git branch
  coworker
  main
* you
```

::: notes
* You're working your *issues*
* Your coworker as well
:::

### Create a new *worktree*

```
❯ git worktree add ../coworker coworker
Preparing worktree (checking out 'coworker')
HEAD is now at e39c79c Coworker
```

::: notes
* Coworker needs you to check its *branch*
* You don't want to mess-up yours
:::

#### Completely separate `worktrees`

```
├── [drwxr-xr-x]  coworker
│   ├── [-rw-r--r--]  .git
│   ├── [-rw-r--r--]  file0
│   ├── [-rw-r--r--]  file1
│   ├── [-rw-r--r--]  file2
│   ├── [-rw-r--r--]  file3
│   └── [-rw-r--r--]  file4
└── [drwxr-xr-x]  repository
    ├── [drwxr-xr-x]  .git
    ├── [-rw-r--r--]  file0
    ├── [-rw-r--r--]  file1
    ├── [-rw-r--r--]  file2
    └── [-rw-r--r--]  file3
```

::: notes
No directories/files overlap
:::

#### But common `git history`

```
❯ /bin/cat coworker/.git
gitdir: [..]/repository/.git/worktrees/coworker
```

::: notes
* Possible to *merge*/*rebase*
* Easy *cherry-pick* in case of *bugfix*
:::

#### Allowing one to code

```
❯ cd coworker
❯ edit file3
❯ git commit -am "Fix typo"
[coworker 1be9375] Fix typo
 1 file changed, 1 insertion(+), 1 deletion(-)
❯ git push
```

#### And keep the initial environment clean

```
❯ cd ../repository
❯ git worktree list
[..]/repository 10b0086 [you]
[..]/coworker   1be9375 [coworker]
❯ git worktree remove ../coworker/
❯ git worktree prune
```

```
[drwxr-xr-x]  .
└── [drwxr-xr-x]  repository
    ├── [drwxr-xr-x]  .git
    ├── [-rw-r--r--]  file0
    ├── [-rw-r--r--]  file1
    ├── [-rw-r--r--]  file2
    └── [-rw-r--r--]  file3
```

# Conclusion

![](https://media.tenor.com/UpxvwdNN3rYAAAAC/modern-problems-solutions.gif)

::: notes
* No silver bullet so don't overuse it
* Perfect for trying out things without messing with the filesystem too much
:::

## See also

* Create a branch on the fly

    ```
    git worktree add -b branch ../foo
    ```
* Editors integration. Yes, even for [the mighty one](https://github.com/ThePrimeagen/git-worktree.nvim) !
* [`Git worktree` documentation](https://git-scm.com/docs/git-worktree)
* [An old blog-post of mine](https://pcoves.gitlab.io/blog/git-worktree/)

## Questions ?

![](data:image/gif;base64,R0lGODlhlACUAJEAAAAAAP///wAAAAAAACH5BAEAAAIALAAAAACUAJQAAAL/jI+py+0Po5y02ouz3rz7D4biSJbmiabqyrbuC8fyTFPAjef6zjO531vwhgDgzkhMHifKpg6J/AmdtyjOSh3ask3odKngYotfLnFrPpfHY21ZGk6rJfBKHeGOJ/FXZt9x51clSGYRaJCX0HVw2NDIN2j4F/H40JgIORdQqVfoOInmCcH5GakY1JlImjkKShiaqTq5yjhru7dRt4joCnirqdtb6hmMq1EMLOzFa7z5C1trupuBLGvaKq3EGi1q+cyNeVGN6nztnd2MHEs+3fbNTKzNDQ1vHY+e3OyOv13PrrwG3D9+AsGU07TvnsJ1BmnBY/hknkR/DeX5SvfuoD16/xoHLjzX7WHBkAeXlRyp7hXKjKQu/XJpTqTMjhVjDoOYcqbJfjRJwvxIMaJOkDhZAhw6MxzSk0ktMqV09ObIUO0m2om6k6i4l1hXGuTJMaxWSfgc9lT6VCydrkuxKTQrJiBZkm6pysn5FK9eo2Xvsj0rh+degk0J5/X7FSRiw4OBHmbMFbFdyVYbC+breLHQGHjZyD2VuEaJzm+E/TQr+lhGz51YpzZBOpXp2aFrjKPiuqi+rX1x54rsJLdXhLzfZuFwO3hpm3eRA686VzfxoAnRcmz+WjHtzMetQvzuPTXaxtNOAwcrntzl3vl8eoxtW33l5x5db/xdOzmGn+Bbj//tCcJ45xVnkoA2rbeZc/kNGN1nBtI1X237yVdYgir5R91nBVII1YIHDmcPasNtmMZk7n0YVHkcuvWgdNYtdyKEKTr14kXTPWaGiVnJ5luGc7UI4ooOLubidjumNRWM6K3FXlwzehjjkQ/CV1eQGPUIWpQaJsmjY9cRaaWWGGYpnI9IMnmjffRhiSB/hMDlFIuYYTcmkGjCuZuN5AU2JJQ6vilkl23yWSeHeFqo5197sicnc9slV+Mwhw7KqHaOXrqZk1dJ2KdxlSaKqZpsAooimRFmGiiAW5qp6Zejminlo6mGmGqVTyKqqo2x5pklpH+RqJyg/y0JWKm0copmkZj/tmQkjMzWlCap7UHXoadehlehndWq+Gq1/Z0Jrq+hSntstLpyGW6ztx5Z3Zq42ppruuOuu6pUFQKrrLXUimstsfhii2O++bpJL6uKYvbvrfxqyy2qpSJI7KLTdlfolQ7L2Ku68rYnnVoZQysirKcmROm79XaM7p8Vx3hfwd5aenHI8ULs7Jy1AoyyyA26HLFm9XWqmYL6ajwwxRsXzaurN5rn87XZgqlak0SHqSnBxeZYnMQS5/yxX1xH+i2/JXON9MQgHwzt1wxe3S6WDR+qtc1SLws12yn/ZzXTGOvX6bdho+10wEribajcLK9G+LwbP9tv33cb/PDIjgt7rs6C/1ve8suVL06j0TS3darMjPfstueSg/505GMm3HW5DCO+suhRwVe16ffiHPqvAFuWe9pZxwmz0KzHbZiJYD++qZiX45i58Tdb7jHvqV88YbdlJv/51XwrPKu7QV+4fO1nW99704BnL37MtpvKPWXJnq792nTaG7/7nK0Nb6u43456CttrXj/lDU93Ivhf/tY3OvjBwICJCxb76EelmpUIZlYjXeP4By7nTRBUuKpg+dBHwNYRSlKFU9zY4CezskVqX1ObHghVRz+lJRB6VJtd52Aou09BsHsbJKHv3rezmQHNhj0k2f4eKEMiXtCHDjRi/0pGPK/VS2/6i9rQIGO+Ftdyroj4uSJ3smjC3dVNeCMq3/dWJ0b7kdFlDOTg637YNXYNEY7Ha5/ymoe8SZXRheQq4RL1JsfY/YyGXXoj9QaHxs01TG3ICp/G8GhFRnrRb5M7GrpSOMdDspGHX7zjIDsgNjPir5J5++QasTcs8JXNgnWMoSpPlko70g54QfSYv2IZwFkm7ZVAdGUvV5lGc22rbm97j/xo6UgYqtCY41OfDlmIRYy1zYHFbObfeCW95J3Rbpu05h4fGMHsiHOc5CynOc+JznSqc53sbKc73wnPeMpzngwoAAA7)

[https://pcoves.gitlab.io/tupper-git_worktree](https://pcoves.gitlab.io/tupper-git_worktree)
